"""
Django settings for news_website project.

Generated by 'django-admin startproject' using Django 1.11.1.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
with open('/home/noticiasDinamicas/brisk-news/secret_key.txt') as f:
    SECRET_KEY = f.read().strip()


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
SECURE_SSL_REDIRECT = True
ALLOWED_HOSTS = [
    'noticiasdinamicas.pythonanywhere.com',
     'www.briskreport.com' ,
    ]
CSRF_COOKIE_SECURE = True #True when httpl enable
CSRF_COOKIE_DOMAIN = 'briskreport.com'
CSRF_TRUSTED_ORIGINS = ['.briskreport.com']
#CSRF_COOKIE_PATH
SESSION_COOKIE_SECURE = True #True when httpl enable
#SESSION_COOKIE_DOMAIN = 'noticiasdinamicas.pythonanywhere.com'
#SESSION_COOKIE_PATH
# Application definition

INSTALLED_APPS = [
	'swnews_app.apps.NewsConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'captcha',
    'ckeditor',
    'ckeditor_uploader',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

]

ROOT_URLCONF = 'news_website.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'swnews_app.templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

            ],
        },
    },
]

WSGI_APPLICATION = 'news_website.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'noticiasDinamica$default',
        'USER': 'noticiasDinamica',
        'PASSWORD': 'EADGBeMySQL3',
        'HOST': 'noticiasDinamicas.mysql.pythonanywhere-services.com',
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = [
    ('es', _('Spanish')),
    ('en', _('English')),
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT =  '/home/noticiasDinamicas/brisk-news/swnews_app/static'


# Redirect to home URL after login (Default redirects to /accounts/profile/)
LOGIN_REDIRECT_URL = '/'

#TEST TO DELETE
#EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
#default = EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#sendgrid
EMAIL_HOST =  'smtp.sendgrid.net'
EMAIL_HOST_USER = 'apikey'
with open('/home/noticiasDinamicas/brisk-news/sendgrid_smtp_key.txt') as f:
    SECRET_KEY_SMTP = f.read().strip()
EMAIL_HOST_PASSWORD = SECRET_KEY_SMTP
EMAIL_PORT = 587
EMAIL_USE_TLS= True

#captcha
CAPTCHA_IMAGE_SIZE = 100,100

CKEDITOR_JQUERY_URL = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'

CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_IMAGE_BACKEND = "pillow"

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': None,
        'disableNativeSpellChecker' :False,
    },
    'richttext_editor': {
        'toolbar': None,

    },
}

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)