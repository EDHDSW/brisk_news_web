"""news_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
#<a href="{% url 'all-news' %}">View All</a>
from django.conf.urls import url,include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from swnews_app import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^accounts/new_user$', views.upload_newUser),
    url(r'^$', views.NewsListHome.as_view(),name = 'home'),
    url(r'^submit_new/$', views.upload_new,name = 'submit_new'),
    url(r'^thanks/$', views.thank_you,name = 'thanks'),
    url(r'^terms/$', views.terms_f,name = 'terms'),
    url(r'^privacy/$', views.privacy_f,name = 'privacy'),
    url(r'^about/$', views.about_f,name = 'about'),
    url(r'^search/$', views.search_view,name = 'search_html'),
    url(r'^news/(?P<pk>\d+)$', views.NewDetailView.as_view(), name='new-detail'),
    url(r'^reporters/(?P<pk>\d+)$', views.ReporterDetailView.as_view(), name='user-detail'),
    url(r'^allNews/$', views.AllNews.as_view(), name='all-news'),
    url(r'^allNews/subs/$', views.AllNewsFollow.as_view(), name='all-subs'),
    url(r'^ajax/unfollows_reporter/$',views.unfollow_reporter,name ='unfollow_reporter'),
    url(r'^ajax/follows_reporter/$',views.follow_reporter,name ='follow_reporter'),
    url(r'^ajax/unfollows_userpage/$',views.unfollow_userpage,name ='unfollow_userpage'),
    url(r'^ajax/deletes_userpage/$',views.delete_userpage,name ='delete_userpage'),
    url(r'^ajax/edits_new/$',views.edit_new,name ='edit_new'),
    url(r'^ajax/deletes_new/$',views.delete_new,name ='delete_new'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^es/$',  views.home_view_es,name = 'eshome'),
    url(r'^en/$',  views.home_view_en,name = 'enhome'),
    #url(r'^home/$',  views.AllNewsFront.as_view(),name = 'secondhome'),
    # url(r'^setLocation/$', views.set_local,name = 'set_location'),
    #url(r'^allNews/world/$', views.AllNewsWorld.as_view(), name='all-news-world'),
    #url(r'^allNews/local/$', views.AllNewsLocal.as_view(), name='all-news-local'),
    #url(r'^allNews/sports/$', views.AllNewsSport.as_view(), name='all-news-sports'),
    #url(r'^allNews/economy/$', views.AllNewsEconomy.as_view(), name='all-news-economy'),
    #url(r'^allNews/weather/$', views.AllNewsWeather.as_view(), name='all-news-weather'),
    #url(r'^allNews/culture/$', views.AllNewsCulture.as_view(), name='all-news-culture'),
    #url(r'^allNews/science/$', views.AllNewsScience.as_view(), name='all-news-science'),
    #url(r'^allNews/oped/$', views.AllNewsEditorial.as_view(), name='all-news-op-ed'),



]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

#url(r'^allNews/(?P<section>\[a-z][^/]+)/$', views.AllNews1.as_view(), name='all-news'),
'''
url(r'^allNews/$', views.AllNews.as_view(), name='all-news-world'),
url(r'^allNews/world/$', views.AllNewsWorld.as_view(), name='all-news-world'),
    url(r'^allNews/local/$', views.AllNewsLocal.as_view(), name='all-news-local'),
    url(r'^allNews/sports/$', views.AllNewsSport.as_view(), name='all-news-sports'),
    url(r'^allNews/economy/$', views.AllNewsEconomy.as_view(), name='all-news-economy'),
    url(r'^allNews/weather/$', views.AllNewsWeather.as_view(), name='all-news-weather'),
    url(r'^allNews/culture/$', views.AllNewsCulture.as_view(), name='all-news-culture'),
    url(r'^allNews/science/$', views.AllNewsScience.as_view(), name='all-news-science'),
'''