# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class New_model(models.Model):
    title = models.CharField(max_length=30,default ='NA')
    hashtags = models.CharField(max_length=30,default ='NA')
    seccion = models.CharField(max_length=30,default ='NA')
    country = models.CharField(max_length=30,default ='NA')
    state = models.CharField(max_length=30,default ='NA')
    city = models.CharField(max_length=30,default ='NA')
    link = models.CharField(max_length=30,default ='NA')
    opinion = models.TextField(null = False,default ='NA')
    def __str__(self):
        return u'%s %s' % (self.seccion, self.title)
	
class Reporter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,unique= True)
    crypto_clabe = models.CharField(null = True,max_length=40,blank=True,verbose_name ='donate to')
    def __str__(self):
		return self.user.username
   
@receiver(post_save, sender=User)
def create_user_reporter(sender, instance, created, **kwargs):
    if created:
        Reporter.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_reporter(sender, instance, **kwargs):
    instance.reporter.save()
# Create your models here.
