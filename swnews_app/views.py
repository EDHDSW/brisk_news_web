# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse,Http404
from django.utils import translation
from django.views.generic import ListView,DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.db.models import Q

from forms import NewsForm,SignUpForm,LocationForm
from swnews_app.models import New_model,Reporter,Link_model
from django.contrib.auth import logout

from random import randint
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.translation import ugettext as _
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import UserPassesTestMixin

translation.activate(translation.get_language())

def home_view(request):
    return render(request,'base.html',{})

def home_view_es(request):
    user_language = 'es'
    translation.activate(user_language)
    request.session[translation.LANGUAGE_SESSION_KEY] = user_language
    return HttpResponseRedirect('/')

def home_view_en(request):
    user_language = 'en'
    translation.activate(user_language)
    request.session[translation.LANGUAGE_SESSION_KEY] = user_language
    return HttpResponseRedirect('/')

def search_view(request):
    errors = []
    if 'search_value' in request.GET and request.GET['search_value']:
        search_value = request.GET['search_value']
        query_title = Q(title__icontains = search_value)
        query_hashtag = Q(hashtags__icontains = search_value)
        query_opinion = Q(opinion__icontains = search_value)
        query_reporter = Q(reporter_name__icontains = search_value)
        query_reporterSpecific = Q(reporter_name = search_value)
        query_location = Q(country = search_value) | Q(state = search_value)| Q(city = search_value)
        #query q objectsQ(question__startswith='Who') | Q(question__startswith='What')
        query_Q = Q()
        boSearch = False
        if 'title' in request.GET:
            query_Q = Q(title__icontains = search_value)
            boSearch = True
        if 'hashtag' in request.GET:
            query_Q = query_Q |query_hashtag
            boSearch = True
        if 'opinion' in request.GET:
            query_Q = query_Q |query_opinion
            boSearch = True
        if 'location' in request.GET:
            query_Q = query_Q |query_location
            boSearch = True
        if 'reporter' in request.GET:
            query_Q = query_Q |query_reporter
            boSearch = True
        if 'reporter_specific' in request.GET:
            query_Q = query_reporterSpecific
            boSearch = True
        if 'all' in request.GET:
            query_Q = query_title | query_hashtag  | query_opinion | query_location | query_reporter
            boSearch = True
        if boSearch == False:
            query_Q = query_title | query_hashtag
        query_search = New_model.objects.filter(query_Q)
        return render(request, 'news_search.html',
                      {'query_search': query_search, 'query': search_value})
    else:
        errors.append(_('Empty search please enter a search term'))

    return render(request, 'news_search.html', {'errors': errors})
def set_local(request):
    if request.method == 'POST':
        form = LocationForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            request.session['country'] = cd.get('country','#NA#')
            request.session['city'] = cd.get('city','#NA#')
            request.session['state'] = cd.get('state','#NA#')
            return HttpResponseRedirect('/')
    else:
        form = LocationForm(initial={'Country': 'Country to seacrh','State':'State','City':'City'})
    return render(request, 'set_location.html', {'form':form})

def upload_new(request):
    novalid_user = False
    if request.user.is_authenticated():
        if request.method == 'POST': #if new submmit
            form = NewsForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                title_form = cd.get('title', 'NoTitle')
                hashtag_form = cd.get('hashtag', '##')
                country_form = cd.get('country', 'NA')
                state_form = cd.get('state', 'NA')
                city_form = cd.get('city', 'NA')
                link_form = cd.get('link', ' NPDNT : No Proof Do Not Trust')
                seccion_form = cd.get('seccion', 'Unknown')
                opinion_form = cd.get('title', 'Unknown')
                richtext_opinion_form = cd.get('richtext_opinion','Unknown')
                if translation.get_language() == 'es': #this validation occurs because the function could return none instead of a valid language
                    language_form = 'es'
                else:
                    language_form = 'en'
                if request.user.is_authenticated():
                    reporter_name = request.user.username
                else:
                    reporter_name = '@@'
                newmodel = New_model(title = title_form, hashtags = hashtag_form,seccion = seccion_form, country = country_form,state = state_form,city = city_form,
                                    link = link_form,opinion = opinion_form,language = language_form,reporter_name = reporter_name,editorial_attr = 0,
                                    richtext_opinion = richtext_opinion_form)
                newmodel.save()
               #add the entry to the database with dictionary cd.get(key, default=None) key  = form entrys
                return HttpResponseRedirect('/thanks/')
        else:
           # form = NewForm() #if first timeq
           form = NewsForm(initial={'link':_('Evidence'),'country':'NA','state':'NA','city':'NA'})
    else:
        return HttpResponseRedirect('/accounts/login/')
    return render(request, 'upload_new.html', {'form':form,'novalid_user':novalid_user})


def upload_newUser(request):
    #if request.method == 'POST': #if new submmit

    if request.method == 'POST': #if new submmit
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.reporter.crypto_clabe = form.cleaned_data.get('crypto_clabe')
            user.save()
            return HttpResponseRedirect('/accounts/login/')
    else:
        form = SignUpForm()
    return render(request, 'upload_new_user.html', {'form':form})
'''


def create_newuser(request):
    novalid_user = False
    if request.method == 'POST': #if new submmit
        form = NewsForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            title_form = cd.get('title', 'NoTitle')
            hashtag_form = cd.get('hastag', '##')
            location_form = cd.get('location', 'Unknown')
            link_form = cd.get('link', ' NPDNT : No Proof Do Not Trust')
            seccion_form = cd.get('seccion', 'Unknown')
            opinion_form = cd.get('title', 'Unknown')
            newmodel = New_model(title = title_form, hashtags = hashtag_form,seccion = seccion_form, location = location_form,link = link_form,opinion = opinion_form)
            newmodel.save()
           #add the entry to the database with dictionary cd.get(key, default=None) key  = form entrys
            return HttpResponseRedirect('/thanks/')
    else:
       # form = NewForm() #if first time
       form = NewsForm(initial={'title': 'I love your site!','link':'Evidence','opinion':'My try to be unbiased opinion'})
    return render(request, 'upload_new.html', {'form':form,'novalid_user':novalid_user})
'''
def thank_you(request):
    return render(request,'thanks.html',{})

def privacy_f(request):
    return render(request,'privacy.html',{})

def terms_f(request):
    return render(request,'terms.html',{})

def about_f(request):
    return render(request,'about.html',{})



class NewsList(ListView):
    model = New_model
    template_name = 'news_list.html'

###########
NEWS_WINDOW_SIZE =10
######
def get_window_range(query_len):
    window_size = NEWS_WINDOW_SIZE
    start = 0
    end = query_len


    if query_len > window_size: #6
        end = query_len-window_size #1
        start = randint(0,end) #0,1
        end = (start + window_size) #4,5

    return start,end     #0 ,4

class NewsListHome_brisk1(ListView):
    template_name = 'news_list.html'
    context_object_name = 'home_news'
  ##paginate_by = 10 or scrollable list
    def get_queryset(self):
        #if request.session.get('language', 'EN') == 'EN':
        #request.session[settings.LANGUAGE_SESSION_KEY]
         if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            return New_model.objects.filter(language = 'en').order_by('-views_count')[:NEWS_WINDOW_SIZE] #main list with the mos clicks
         else:
            return New_model.objects.filter(language = 'es').order_by('-views_count')[:NEWS_WINDOW_SIZE] #main list with the mos clicks
        #return New_model.objects.all().order_by('-views_count')[:20] #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(NewsListHome, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        #news =  New_model.objects.all()
        #unpaid_invoices = invoices.select_related('vendor').filter(status='UNPAID')
        #New_model.objects.order_by("title")
        #qs2 =New_model.objects.filter(seccion = 'world')[:50]
        #qs3 = New_model.objects.filter(seccion = 'world')[:50]
        #qs1.union(qs2, qs3) o Q objects Q()1Q()
        #qs = New_model.objects.filter( Q(seccion= 'world')|Q() )

        #context['lenguage'] = self.request.session[translation.LANGUAGE_SESSION_KEY]
        '''
        #newst
        qs1 = New_model.objects.filter(seccion = 'world').order_by('timestamp')[:10]
        #oldest
        qs2 = New_model.objects.filter(seccion = 'world').order_by('-timestamp')[:10]
        #views
        qs3 = New_model.objects.filter(seccion = 'world').order_by('views_count')[:10]
        #random
        qs4 = New_model.objects.filter(seccion = 'world')[world_s:world_s+10]
        context['world_list'] =qs1.union(qs2,qs3,qs4)
        start,end = get_window_range(New_model.objects.all().count())
        context['main_list_random'] = New_model.objects.all()[start:end]
        '''

        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'

        #Main New
        try:
            context['main_new'] = New_model.objects.filter(language = current_language).get(editorial_attr = 1)
        except New_model.DoesNotExist:
            context['main_new'] = None
        #context['main_new'] = New_model.objects.filter(language = current_language).get(editorial_attr = 1)

        #random window
        start,end = get_window_range(New_model.objects.filter(language = current_language).filter(seccion = 'world').count())
        context['world_list'] = New_model.objects.filter(language = current_language).filter(seccion = 'world')[start:end]


        country = self.request.session.get('country','#NA#')
        state =   self.request.session.get('state','#NA#')
        city =    self.request.session.get('city','#NA#')
        query_Q = Q()
        boLocal = False
        if country != '#NA#':
            query_Q = Q(title__icontains = country)
            boLocal = True
        if state != '#NA#':
            query_Q = query_Q|Q(title__icontains = state)
            boLocal = True
        if city != '#NA#':
            query_Q = query_Q|Q(title__icontains = city)
            boLocal = True
        if boLocal == False:
            query_Q = Q(seccion = 'local')
        '''#random window
        #start,end = get_window_range(New_model.objects.filter(language = current_language).filter(seccion = 'local').count())
        #context['local_list'] = New_model.objects.filter(language = current_language).filter(seccion = 'local')
        #viral
        #context['local_list_viral'] =New_model.objects.filter(language = current_language).filter(seccion = 'local').order_by('-views_count')[:20]
        '''
        ################
        #random window
        start,end = get_window_range(New_model.objects.filter(language = current_language).filter(query_Q).count())
        context['local_list'] = New_model.objects.filter(language = current_language).filter(query_Q)[start:end]

        #random window
        start,end = get_window_range(New_model.objects.filter(language = current_language).filter(seccion = 'sport').count())
        context['sports_list'] = New_model.objects.filter(language = current_language).filter(seccion = 'sport')[start:end]
 #random window
        start,end = get_window_range(New_model.objects.filter(language = current_language).filter(seccion = 'economy').count())
        context['economy_list'] = New_model.objects.filter(language = current_language).filter(seccion = 'economy')[start:end]

        #random window
        start,end = get_window_range(New_model.objects.filter(language = current_language).filter(seccion = 'weather').count())
        context['weather_list'] = New_model.objects.filter(language = current_language).filter(seccion = 'weather')[start:end]

        #random window
        start,end = get_window_range(New_model.objects.filter(language = current_language).filter(seccion = 'sciene').count())
        context['science_list'] = New_model.objects.filter(language = current_language).filter(seccion = 'sciene')[start:end]

        #random window
        start,end = get_window_range(New_model.objects.filter(language = current_language).filter(seccion = 'culture').count())
        context['culture_list'] = New_model.objects.filter(language = current_language).filter(seccion = 'culture')[start:end]
        #viral
        #context['culture_list_viral'] =New_model.objects.filter(language = current_language).filter(seccion = 'culture').order_by('-views_count')[:NEWS_WINDOW_SIZE]

        #Opinio Editorial
        context['opinion_list'] =New_model.objects.filter(language = current_language).filter(editorial_attr = 2).order_by('-views_count')[:NEWS_WINDOW_SIZE]

        # FOLLOWING
        if self.request.user.is_authenticated():
            user_following = self.request.user.reporter.following;
            query_Q = Q()
            if user_following:
                following = user_following.split('@#')
                for follow in following:
                    if follow:
                        query_Q = query_Q|Q(reporter_name=follow)
                context['follow_list'] = New_model.objects.filter(query_Q)[:NEWS_WINDOW_SIZE]

        return context

class NewsListHome(ListView):
    template_name = 'news_list.html'
    context_object_name = 'home_news'
  ##paginate_by = 10 or scrollable list
    def get_queryset(self):
        #if request.session.get('language', 'EN') == 'EN':
        #request.session[settings.LANGUAGE_SESSION_KEY]
         if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            start,end = get_window_range(New_model.objects.filter(language = 'en').count())
            return New_model.objects.filter(language = 'en').order_by('-views_count')[start:end] #main list with the mos clicksreturn New_model.objects.filter(language = 'en').order_by('-views_count')[:NEWS_WINDOW_SIZE]
         else:
            start,end = get_window_range(New_model.objects.filter(language = 'es').count())
            return New_model.objects.filter(language = 'es').order_by('-views_count')[start:end] #main list with the mos clicks
        #return New_model.objects.all().order_by('-views_count')[:20] #main list with the mos clicks
    def get_context_data(self, **kwargs):
        context = super(NewsListHome, self).get_context_data(**kwargs)
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'

        #Main New
        try:
            context['main_news']= Link_model.objects.filter(language = current_language).order_by('editorial_attr') #main list with the mos clicks
        except New_model.DoesNotExist:
            context['main_news'] = None

        # FOLLOWING
        if self.request.user.is_authenticated():
            user_following = self.request.user.reporter.following;
            query_Q = Q()
            if user_following:
                following = user_following.split('@#')
                for follow in following:
                    if follow:
                        query_Q = query_Q|Q(reporter_name=follow)
                context['follow_list'] = New_model.objects.filter(query_Q)[:NEWS_WINDOW_SIZE]

        return context

PAGINATE_BY_SIZE = 10
class AllNewsWorld(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(seccion = 'world').order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsWorld, self).get_context_data(**kwargs)
        context['title'] = _('World News')
        return context

class AllNewsLocal(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(seccion = 'local').order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsLocal, self).get_context_data(**kwargs)
        context['title'] = _('Local News')
        return context

class AllNewsSport(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(seccion = 'sport').order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsSport, self).get_context_data(**kwargs)
        context['title'] = _('Sport News')
        return context

class AllNewsEconomy(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(seccion = 'economy').order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsEconomy, self).get_context_data(**kwargs)
        context['title'] = _('Economy News')
        return context

class AllNewsWeather(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(seccion = 'weather').order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsWeather, self).get_context_data(**kwargs)
        context['title'] = _('Weather News')
        return context

class AllNewsCulture(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(seccion = 'culture').order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsCulture, self).get_context_data(**kwargs)
        context['title'] = _('Culture News')
        return context

class AllNewsScience(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(seccion = 'sciene').order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsScience, self).get_context_data(**kwargs)
        context['title'] = _('Sciene News')
        return context

class AllNewsEditorial(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).filter(editorial_attr = 2).order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsEditorial, self).get_context_data(**kwargs)
        context['title'] = _('Opinion Editorial')
        return context

class AllNews(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return New_model.objects.filter(language = current_language).order_by('-views_count') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNews, self).get_context_data(**kwargs)
        context['title'] = _('All News')
        return context

class AllNewsFollow(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = PAGINATE_BY_SIZE
    def get_queryset(self):
        query_Q = Q()
        if self.request.user.is_authenticated():
            user_following = self.request.user.reporter.following;
            if user_following:
                following = user_following.split('@#')
                for follow in following:
                    if follow:
                        query_Q = query_Q|Q(reporter_name=follow)
                return New_model.objects.filter(query_Q)
        return New_model.objects.none()
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsFollow, self).get_context_data(**kwargs)
        context['title'] =_( 'Subs')
        return context



@login_required(login_url='/accounts/login/')
def follow_reporter(request):
    if request.is_ajax() and request.POST:
        ##add to database
        reporter_name = request.POST.get('reporter')
        user_following = request.user.reporter.following;

        #new_following = ""
        #request.user.reporter.following = new_following
        #request.user.save()

        if user_following:
            new_following = request.user.reporter.following + "@#"+reporter_name;
        else:
            new_following = "@#"+reporter_name;
        request.user.reporter.following = new_following
        request.user.save()
        data = {'message':_( "you follow the reporter %s " )% reporter_name}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404

@login_required(login_url='/accounts/login/')
def unfollow_reporter(request):
    if request.is_ajax() and request.POST:
        ##update database
        reporter_name = request.POST.get('reporter')
        new_following = ""
        #request.user.reporter.following = new_following
        #request.user.save()
        #'''
        if request.user.is_authenticated():
            user_following = request.user.reporter.following;
            if user_following:
                following = user_following.split('@#')
                for follow in following:
                    if follow:
                        if follow != reporter_name:
                            new_following = '@#'+ follow + new_following
                request.user.reporter.following = new_following
                request.user.save()   #'''
        data = {'message': _("you un-follow the reporter %s ") % reporter_name}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404

@login_required(login_url='/accounts/login/')
def unfollow_userpage(request):
    if request.is_ajax() and request.POST:
        ##update database
        reporter_names = json.loads(request.POST.get('reporter_names'))
        new_following = ""
        found_flag = 0
        user_following = request.user.reporter.following;
        if user_following:
            following = user_following.split('@#')
            for follow in following:
                if follow:
                    for reporter_name in reporter_names:
                        if follow == reporter_name:
                            found_flag = 1
                            break
                    if found_flag == 0:
                        new_following = '@#'+ follow + new_following
                    found_flag = 0
            request.user.reporter.following = new_following
            request.user.save()
        data = {'message': "success"}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404

@login_required(login_url='/accounts/login/')
def delete_userpage(request):
    if request.is_ajax() and request.POST:
        ##update database
        reporter_name =request.user.username # json.loads(request.POST.get('user_name'))
        logout(request)
        try:
            New_model.objects.filter(reporter_name=reporter_name).delete()
        except Exception as e:
            data = {'message': "news error"}
        try:
            u = User.objects.get(username = reporter_name)
            u.delete()
            data = {'message': "success"}
        except User.DoesNotExist:
            data = {'message': "error"}
        except Exception as e:
            data = {'message': "error"}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404

@login_required(login_url='/accounts/login/')
def edit_new(request):
    if request.is_ajax() and request.POST:
        data = {'message': "error"}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404

@login_required(login_url='/accounts/login/')
def delete_new(request):
    if request.is_ajax() and request.POST:
        new_ids = request.POST.get('new_id')
        try:
            New_model.objects.get(pk=new_ids).delete()
        except Exception as e:
            data = {'message': "error"}
        data = {'message': "error"}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404


class NewDetailView(DetailView):
    model = New_model
    template_name = 'new_detail.html'
    def get_object(self, queryset=None):
        object = super(NewDetailView,

        self).get_object(queryset=queryset)
        object.views_count+=1
        object.save()
        return object
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(NewDetailView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            reporter_name = self.object.reporter_name
            if reporter_name == '@@':##this mean an anonymus reporter
                context['following_command'] = 1 #not follow
            elif reporter_name == self.request.user.username:##same user
                context['following_command'] = 1 #not follow
            else:
                user_following = self.request.user.reporter.following
                if user_following:
                    following = user_following.split('@#')
                    context['following_command'] = 2 # follow
                    for follow in following:
                        if follow == reporter_name:
                            context['following_command'] = 3 #unfollow
                            break
                else:
                    context['following_command'] = 2 #follo3
        else:
            context['following_command'] = 1 #not follow
        context['hashtagList'] = self.object.hashtags.split('#')
        return context


class ReporterDetailView(LoginRequiredMixin,DetailView):
    model = User
    template_name = 'user_detail.html'
    login_url = '/accounts/login/'
   # def test_func(self):
    #    return self.request.user.username == self.username
    #def dispatch(self, request, *args, **kwargs):
     #   if (self.request.user.id != self.object.id):
      #      HttpResponseRedirect('/accounts/login/')
   # def get_object(self):
    #    super(ReporterDetailView, self).get_object(self)
     #   HttpResponseRedirect('/accounts/login/')
    def get(self, request, **kwargs):
        self.object = self.get_object()
        if (self.request.user.id != self.object.id):
            return HttpResponseRedirect('/accounts/login/')
        else:
            context = self.get_context_data(object=self.object)
            return self.render_to_response(context)
    def get_context_data(self, **kwargs):
        context = super(ReporterDetailView, self).get_context_data(**kwargs)
        if (self.request.user.id != self.object.id):
            redirect('/accounts/login/')
            #return context
        else:
            context = super(ReporterDetailView, self).get_context_data(**kwargs)
            user_following = self.object.reporter.following
            if user_following:
                split = user_following.split('@#');
                if split:
                    split.pop(0)
                context['following_split'] = split
                context['jason_split'] = json.dumps(list(split), cls=DjangoJSONEncoder)
            else:
                context['following_split'] = None
                context['jason_split'] = None
            try:
                context['news_list'] = New_model.objects.filter(reporter_name=self.object.username)
            except New_model.DoesNotExist:
                context['news_list'] = None

            return context



''' brisk 2.0'''
class AllNewsFront(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    def get_queryset(self):
        if translation.get_language() == 'en':#this validation occurs because the function could return none instead of a valid language
            current_language = 'en'
        else:
            current_language = 'es'
        return Link_model.objects.filter(language = current_language).order_by('editorial_attr') #main list with the mos clicks
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNewsFront, self).get_context_data(**kwargs)
        context['title'] = _('All News ')
        return context
''' brisk 2.0'''

'''
class AllNews(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = 10
    def get_queryset(self):
        section = self.request.GET.get('section')
        if section == 'world':
            return New_model.objects.filter(seccion = 'world').order_by('-views_count')
        elif section == 'local':
            return New_model.objects.filter(seccion = 'local').order_by('-views_count')
        elif section == 'sport':
            return New_model.objects.filter(seccion = 'sport').order_by('-views_count')
        elif section == 'economy':
            return New_model.objects.filter(seccion = 'economy').order_by('-views_count')
        elif section == 'weather':
            return New_model.objects.filter(seccion = 'weather').order_by('-views_count')
        elif section == 'culture':
            return New_model.objects.filter(seccion = 'culture').order_by('-views_count')
        elif section == 'sciene':
            return New_model.objects.filter(seccion = 'sciene').order_by('-views_count')
        else:
            return New_model.objects.filter(seccion = 'world').order_by('-views_count')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNews, self).get_context_data(**kwargs)
        section = self.request.GET.get('section')
        if section == 'world':
            context['title'] = 'World News'
        elif section == 'local':
            context['title'] = 'Local News'
        elif section == 'sport':
            context['title'] = 'Sport News'
        elif section == 'economy':
            context['title'] = 'Economy News'
        elif section == 'weather':
            context['title'] = 'Weather News'
        elif section == 'culture':
            context['title'] = 'Culture News'
        elif section == 'sciene':
            context['title'] = 'Science News'
        else:
            context['title'] = 'NA'
        return context

class NewDetailView(DetailView):
    model = New_model
    template_name = 'new_detail.html'
    def get_object(self, queryset=None):
        object = super(NewDetailView,

        self).get_object(queryset=queryset)
        object.views_count+=1
        object.save()
        return object
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(NewDetailView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            reporter_name = self.object.reporter_name
            if reporter_name == '@@':##this mean an anonymus reporter
                context['following_command'] = "notfollow"
            else:
                user_following = self.request.user.reporter.following;
                if user_following:
                    following = user_following.split('@#')
                    context['following_command'] = "follow"
                    for follow in following:
                        if follow == reporter_name:
                            context['following_command'] = "unfollow"
                            break
                else:
                    context['following_command'] = "follow"
        else:
            context['following_command'] = "notfollow"
        context['hashtagList'] = self.object.hashtags.split('#')
        return context

class AllNews(ListView):
    template_name = 'all_news.html'
    context_object_name = 'all_news'
    paginate_by = 2
    def get_queryset(self):
        section = self.request.GET.get('section')
        if section == 'world':
            return New_model.objects.filter(seccion = 'world').order_by('-views_count')
        elif section == 'local':
            return New_model.objects.filter(seccion = 'local').order_by('-views_count')
        elif section == 'sport':
            return New_model.objects.filter(seccion = 'sport').order_by('-views_count')
        elif section == 'economy':
            return New_model.objects.filter(seccion = 'economy').order_by('-views_count')
        elif section == 'weather':
            return New_model.objects.filter(seccion = 'weather').order_by('-views_count')
        elif section == 'culture':
            return New_model.objects.filter(seccion = 'culture').order_by('-views_count')
        elif section == 'sciene':
            return New_model.objects.filter(seccion = 'sciene').order_by('-views_count')
        else:
            return New_model.objects.filter(seccion = 'world').order_by('-views_count')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AllNews, self).get_context_data(**kwargs)
        section = self.request.GET.get('section')
        if section == 'world':
            context['title'] = 'World News'
        elif section == 'local':
            context['title'] = 'Local News'
        elif section == 'sport':
            context['title'] = 'Sport News'
        elif section == 'economy':
            context['title'] = 'Economy News'
        elif section == 'weather':
            context['title'] = 'Weather News'
        elif section == 'culture':
            context['title'] = 'Culture News'
        elif section == 'sciene':
            context['title'] = 'Science News'
        else:
            context['title'] = 'NA'
        return context

 '''

