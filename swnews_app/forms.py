from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from swnews_app.models import Reporter
from captcha.fields import CaptchaField
from ckeditor.widgets import CKEditorWidget
from django.utils.translation import gettext_lazy as _
from django.utils import translation
translation.activate(translation.get_language())

class SearchForm(forms.Form):
    title =  forms.CharField(label='News title',max_length=100)

class LocationForm(forms.Form):
    country = forms.CharField(label='country',max_length=30,required=False)
    state = forms.CharField(label='state',max_length=30,required=False)
    city = forms.CharField(label='city',max_length=30,required=False)

class NewsForm(forms.Form):
    WORLD=_('world')
    LOCAL =_('local')
    SPORT = _('sport')
    ECONOMY = _('economy')
    WEATHER = _('weather')
    CULTURE = _('culture')
    SCIENCE = _('sciene')

    SECCIONES = (
    (WORLD, _('World')),
    (LOCAL,_('Local(country,city...)')),
    (SPORT,_('Sports')),
    (ECONOMY,_('Economy')),
    (WEATHER,_('Weather')),
    (CULTURE,_('Culture(Art,theater ,movies..)')),
    (SCIENCE,_('Science')),
    )

    title =  forms.CharField(label=_('News title'),max_length=100)
    hashtag = forms.CharField(required=False,max_length=30)
    country = forms.CharField(required=False,label=_('Country'),max_length=50)
    state = forms.CharField(required=False,label=_('State/county'),max_length=50)
    city = forms.CharField(required=False,label=_('City/Town'),max_length=50)
    link = forms.URLField(max_length=200)
    #seccion = forms.CharField(max_length=30)
    seccion = forms.ChoiceField(widget=forms.Select,choices=SECCIONES)
    #opinion = forms.CharField(widget=forms.Textarea)
    richtext_opinion =  forms.CharField(widget=CKEditorWidget(),label=_('Opinion'))
    captcha = CaptchaField()
    accept_terms = forms.BooleanField(label= _("Accepts Terms & conditions"),required =True)

    def clean_title(self):
        title = self.cleaned_data['title']
        num_words = len(title.split())
        if num_words < 2:
            raise forms.ValidationError(_("Not enough words!"))

        return title

class SignUpForm(UserCreationForm):

    crypto_clabe = forms.CharField(widget=forms.Textarea,max_length=2000)
    captcha = CaptchaField()
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',"crypto_clabe","captcha" )

    def clean_crypto_clabe(self):
        cleaned_data = super(SignUpForm, self).clean()
        crypto = self.cleaned_data.get("crypto_clabe")
        size = len(crypto)
        if size < 7:
            raise forms.ValidationError("Incorrect crypto clabe")
        return crypto

#https://docs.djangoproject.com/en/1.11/ref/forms/widgets/#django.forms.SelectMultiple
        '''
class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

class ReporterForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # magic
        self.user = kwargs['instance'].user
        user_kwargs = kwargs.copy()
        user_kwargs['instance'] = self.user
        self.uf = UserForm(*args, **user_kwargs)
        # magic end

        super(ReporterForm, self).__init__(*args, **kwargs)

        self.fields.update(self.uf.fields)
        self.initial.update(self.uf.initial)


    def save(self, *args, **kwargs):
        # save both forms
        self.uf.save(*args, **kwargs)
        return super(ReporterForm, self).save(*args, **kwargs)

    class Meta:
        model = Reporter
        fields = ("crypto_clabe",)


'''
'''
class UserReporterCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")


class ReporterForm(forms.ModelForm):
    class Meta:
        model = Reporter
        fields = ("crypto_clabe",)

    def __init__(self, *args, **kwargs):
        user_kwargs = kwargs.copy()
        if kwargs.has_key('instance'):
            self.user = kwargs['instance'].user
            user_kwargs['instance'] = self.user
        self.user_form = UserReporterCreateForm(*args, **user_kwargs)

        super(ReporterForm, self).__init__(*args, **kwargs)
        self.fields.update(self.user_form.fields)
        self.initial.update(self.user_form.initial)

    def clean_crypto_clabe(self):
        cleaned_data = super(ReporterForm, self).clean()
        crypto = self.cleaned_data.get("crypto_clabe")
        size = len(crypto)
        if size < 7:
            raise forms.ValidationError("Incorrect crypto clabe")
        return crypto

    def save(self, commit=True):
        self.user_form.save(commit)
        return super(ReporterForm, self).save(commit)

'''

'''
class ReporterForm(forms.ModelForm):
    class Meta:
        model = Reporter
        fields = ("crypto_clabe",)
    def clean_crypto_clabe(self):
        cleaned_data = super(ReporterForm, self).clean()
        crypto = self.cleaned_data.get("crypto_clabe")
        size = len(crypto)
        if size < 7:
            raise forms.ValidationError("Incorrect crypto clabe")
        return crypto
    def save(self, commit=True):
        return super(ReporterForm, self).save(commit)

class UserReporterCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def __init__(self, *args, **kwargs):
        super(UserReporterCreateForm, self).__init__(*args, **kwargs)
        reporter_kwargs = kwargs.copy()
        if kwargs.has_key('instance'):
            self.reporter = kwargs['instance'].reporter
            reporter_kwargs['instance'] = self.reporter
        self.reporter_form = ReporterForm(*args, **reporter_kwargs)
        self.fields.update(self.reporter_form.fields)
        self.initial.update(self.reporter_form.initial)
    def clean(self):
        cleaned_data = super(UserReporterCreateForm, self).clean()
        self.errors.update(self.reporter_form.errors)
        return cleaned_data

    def save(self, commit=True):
       super(UserReporterCreateForm, self).save(commit)
        #self.reporter_form.save(commit)
        return super(UserReporterCreateForm, self).save(commit)
class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
'''