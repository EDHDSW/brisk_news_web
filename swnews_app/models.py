# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from ckeditor.fields import RichTextField

class New_model(models.Model):
    title = models.CharField(max_length=100,default ='NA')
    hashtags = models.CharField(max_length=30,default ='NA')
    seccion = models.CharField(max_length=50,default ='NA')
    country = models.CharField(max_length=50,default ='NA')
    state = models.CharField(max_length=50,default ='NA')
    city = models.CharField(max_length=50,default ='NA')
    link = models.CharField(max_length=200,default ='NA')
    opinion = models.TextField(null = False,default ='NA')
    created_at = models.DateField(auto_now_add=True)
    timestamp = models.DateTimeField(auto_now_add=True, )
    views_count = models.IntegerField(default = 0);
    language = models.CharField(max_length=20,default = 'en');
    reporter_name = models.CharField(max_length=100,default ='Anon')
    editorial_attr = models.IntegerField(default = 0); #0, normal 1, main , 2 op-edge
    richtext_opinion = RichTextField(config_name='richttext_editor',null = False,default ='NA')
    def __str__(self):
        return u'%s %s' % (self.seccion, self.title)
    def __unicode__(self):
        return u'%s %s' % (self.seccion, self.title)

class Link_model(models.Model):
    title = models.CharField(max_length=100,default ='NA')
    link = models.CharField(max_length=200,default ='NA')
    created_at = models.DateField(auto_now_add=True)
    timestamp = models.DateTimeField(auto_now_add=True, )
    click_count = models.IntegerField(default = 0);
    language = models.CharField(max_length=20,default = 'en');
    editorial_attr = models.IntegerField(default = 0); #Order of appereance
    def __str__(self):
        return u'%s %s' % (self.language, self.title)
    def __unicode__(self):
        return u'%s %s' % (self.language, self.title)

class Reporter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,unique= True)
    crypto_clabe = models.CharField(max_length=2000,blank=True,verbose_name ='donate to',default ='NA')
    following = models.CharField(null = True,max_length=800,blank=True,verbose_name ='following')
    def __str__(self):
		return self.user.username

@receiver(post_save, sender=User)
def create_user_reporter(sender, instance, created, **kwargs):
    if created:
        Reporter.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_reporter(sender, instance, **kwargs):
    instance.reporter.save()
# Create your models here.
