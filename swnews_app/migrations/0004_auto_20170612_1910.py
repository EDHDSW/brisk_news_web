# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-13 00:10
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('swnews_app', '0003_auto_20170529_2059'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='new',
            new_name='New_model',
        ),
    ]
