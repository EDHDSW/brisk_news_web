# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-08-09 20:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('swnews_app', '0009_auto_20170710_1759'),
    ]

    operations = [
        migrations.AddField(
            model_name='new_model',
            name='language',
            field=models.CharField(default='en', max_length=10),
        ),
    ]
