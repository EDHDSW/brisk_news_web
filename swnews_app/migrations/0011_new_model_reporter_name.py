# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-10-17 01:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('swnews_app', '0010_new_model_language'),
    ]

    operations = [
        migrations.AddField(
            model_name='new_model',
            name='reporter_name',
            field=models.CharField(default='Anon', max_length=30),
        ),
    ]
