# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import New_model, Reporter,Link_model
from ckeditor.fields import RichTextField



class NewsAdmin(admin.ModelAdmin):
    list_display = ('title','editorial_attr','language','reporter_name','views_count', 'seccion', 'country', 'state', 'city','link','hashtags',)#language 'en','es',edd att normal 0  main 1 ,  op-edge 2
    search_fields = ('title', 'seccion','hashtags','reporter_name')
    ordering  = ('-timestamp',)

    #list_filter = ('date',)
    #fields = ('title', 'authors', 'publisher','richtext_opinion')

class LinksAdmin(admin.ModelAdmin):
    list_display = ('title','link','editorial_attr','language','click_count','created_at','timestamp')#language 'en','es',edd att normal 0  main 1 ,  op-edge 2
    search_fields = ('title','link')
    ordering  = ('editorial_attr',)

    #list_filter = ('date',)
    #fields = ('title', 'authors', 'publisher','richtext_opinion')
# Define an inline admin descriptor for Reporter model
# which acts a bit like a singleton
class ReporterInline(admin.StackedInline):
    model = Reporter
    can_delete = False
    verbose_name_plural = 'reporter'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (ReporterInline, )


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
# Register your models here.
admin.site.register(New_model,NewsAdmin)
admin.site.register(Link_model,LinksAdmin)
